# Mini project - Backend

This is the backend for our mini-project application.

Your goal for this part of the mini project:

- Fork this repo on GitLab. Make sure your fork is public.
- Create the file needed for the GitLab CI pipeline
- Create a pipeline with the following stages and jobs:
    - stage: `code_quality`
        - job: make sure the code is PEP8 compliant
        - job: make sure the code is linted
    - stage: `unit_tests`
        - job: run the tests with `pytest`. Generate a coverage report for the
          tests.
    - stage: `build`
        - job: build and push the docker image for the backend to the repo's
          registry. Make sure to create 2 tags for each run of the pipeline:
            - `latest`
            - the commit SHA digest
          this job should run only on your default branch (usually `main` or
          `master`), either on commits or merge requests targeting the branch

Once you are done with your pipeline check out the deployment repo and make the
`backend` service use your image using the `latest` tag.
